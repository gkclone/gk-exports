<?php

/**
 * General settings form.
 */
function gk_exports_settings_form() {
  // Sitewide source field used in all GK CSVs.
  $form['gk_exports_source'] = array(
    '#type' => 'textfield',
    '#title' => 'Source',
    '#default_value' => variable_get('gk_exports_source', ''),
  );

  // Make it possible to enable/disable the automated exports.
  $form['gk_exports_automation_enable'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable automation',
    '#default_value' => variable_get('gk_exports_automation_enable', 0),
  );

  return system_settings_form($form);
}

/**
 * Callback for the build handler log page.
 */
function gk_exports_build_handler_log_page_callback($build_handler_machine_name) {
  // Output a table.
  $header = array(
    'lid' => 'Log ID',
    'build_options' => 'Build options',
    'build_start' => 'Build start',
    'build_stop' => 'Build stop',
    'delivery_handler' => 'Delivery handler',
    'delivery_options' => 'Delivery options',
    'delivery_start' => 'Delivery start',
    'delivery_stop' => 'Delivery stop',
  );

  $rows = array();

  // Get alist of delivery handlers so we can show the pretty name in the table.
  $delivery_handlers = gk_exports_delivery_handlers();

  // Get all the logs for this build handler.
  $query = db_select('gk_exports_log', 'l')->fields('l')
    ->condition('l.build_handler', $build_handler_machine_name)
    ->orderBy('l.lid', 'DESC');

  if ($logs = $query->execute()->fetchAll()) {
    foreach ($logs as $log) {
      $delivery_handler = $log->delivery_handler;

      if (isset($delivery_handlers[$log->delivery_handler])) {
        $delivery_handler = $delivery_handlers[$log->delivery_handler]['class']::getName();
      }

      $rows[] = array(
        'lid' => $log->lid,
        'build_options' => $log->build_options,
        'build_start' => date('d/m/Y - H:i:s', $log->build_start),
        'build_stop' => date('d/m/Y - H:i:s', $log->build_stop),
        'delivery_handler' => $delivery_handler,
        'delivery_options' => $log->delivery_options,
        'delivery_start' => date('d/m/Y - H:i:s', $log->delivery_start),
        'delivery_stop' => date('d/m/Y - H:i:s', $log->delivery_stop),
      );
    }
  }

  return array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => 'No logs were found for this build handler.',
  );
}

/**
 * Form for invoking a build handler and delivering the result.
 */
function gk_exports_build_handler_run_form($form, &$form_state, $build_handler_machine_name) {
  // Get more information about the specified build handler.
  $build_handler = gk_exports_get_build_handler($build_handler_machine_name);

  // Display the options form for the specified build handler.
  $form['build'] = array(
    '#type' => 'fieldset',
    '#title' => 'Build',
    '#tree' => TRUE,
  );

  $form['build']['handler'] = array(
    '#type' => 'value',
    '#value' => $build_handler_machine_name,
  );

  if ($build_handler_form = $build_handler['class']::optionsForm()) {
    $form['build']['options'] = $build_handler_form;
  }
  else {
    $form['build']['options']['#description'] = t('No options are available for the %name build handler.', array(
      '%name' => $build_handler['class']::getName(),
    ));
  }

  // Use the delivery handler select helper form to allow selection via AJAX.
  $form['delivery'] = array(
    '#type' => 'fieldset',
    '#title' => 'Delivery',
    '#tree' => TRUE,
  );

  $form_state['gk_exports']['delivery_handler_ajax_select']['parents'] = array('delivery');
  gk_exports_delivery_handler_ajax_select_form($form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Export',
  );

  return $form;
}

/**
 * Validate handler for the run form.
 */
function gk_exports_build_handler_run_form_validate($form, &$form_state) {
  // Check that a delivery handler was selected.
  $delivery_parents = $form_state['gk_exports']['delivery_handler_ajax_select']['parents'];
  $delivery_values = drupal_array_get_nested_value($form_state['values'], $delivery_parents);

  if (empty($delivery_values['ajax_wrapper']['handler'])) {
    form_set_error(implode('][', $delivery_parents) . '][ajax_wrapper][handler', 'Please select a delivery handler.');
  }
  else {
    // Might as well save this to a known area of the form state so we don't
    // have to call drupal_array_get_nested_value() in the submit handler.
    $form_state['gk_exports']['delivery_values'] = $delivery_values['ajax_wrapper'];
  }
}

/**
 * Submit handler for the run form.
 */
function gk_exports_build_handler_run_form_submit($form, &$form_state) {
  gk_exports_export($form_state['values']['build'], $form_state['gk_exports']['delivery_values']);
}

/**
 * Form for configuring build handler specific options.
 */
function gk_exports_build_handler_settings_form($form, &$form_state, $build_handler_machine_name) {
  // Get more information about the specified build handler.
  $build_handler = gk_exports_get_build_handler($build_handler_machine_name);

  // Check for default values.
  $variable_name = 'gk_exports_build_handler_settings__' . $build_handler_machine_name;
  $defaults = variable_get($variable_name);

  $form[$variable_name] = array(
    '#tree' => TRUE,
  );

  // Settings for automating this export.
  $form[$variable_name]['automation'] = array(
    '#type' => 'fieldset',
    '#title' => 'Automation',
  );

  $form[$variable_name]['automation']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable automated exports for the %name build handler', array(
      '%name' => $build_handler['class']::getName(),
    )),
    '#default_value' => !empty($defaults['automation']['enabled']),
  );

  // Use the delivery handler select helper form to allow selection via AJAX.
  $form[$variable_name]['automation']['delivery'] = array(
    '#type' => 'fieldset',
    '#title' => 'Delivery',
  );

  $form_state['gk_exports']['delivery_handler_ajax_select'] = array(
    'parents' => array(
      $variable_name, 'automation', 'delivery',
    ),
    'defaults' => isset($defaults['automation']['delivery']['ajax_wrapper']) ?
      $defaults['automation']['delivery']['ajax_wrapper'] : array(),
  );
  gk_exports_delivery_handler_ajax_select_form($form, $form_state);

  return system_settings_form($form);
}

/**
 * Provides a reusable AJAX select element for selecting a delivery handler.
 */
function gk_exports_delivery_handler_ajax_select_form(&$form, &$form_state) {
  // Don't do anything if we've not been told where to put the element.
  if (!isset($form_state['gk_exports']['delivery_handler_ajax_select']['parents'])) {
    return;
  }

  // Retrieve the part of the form we'll be embedding the element in.
  $parents = $form_state['gk_exports']['delivery_handler_ajax_select']['parents'];

  if (!$subform = &drupal_array_get_nested_value($form, $parents)) {
    return;
  }

  // Check to see if we have default values.
  $defaults = array();

  if (isset($form_state['gk_exports']['delivery_handler_ajax_select']['defaults'])) {
    $defaults = $form_state['gk_exports']['delivery_handler_ajax_select']['defaults'];
  }

  $defaults += array(
    'handler' => NULL,
    'options' => array(),
  );

  // This part of the form gets replaced when the AJAX request completes.
  $subform['ajax_wrapper'] = array(
    '#theme_wrappers' => array('container'),
    '#attributes' => array(
      'id' => 'ajax-wrapper--delivery',
    ),
  );

  // Determine a default value for the select element.
  $delivery_handler_default = $defaults['handler'];

  if (!empty($form_state['values'])) {
    $values = drupal_array_get_nested_value($form_state['values'], $parents);
    $delivery_handler_default = $values['ajax_wrapper']['handler'];
  }

  // Build a list of options for the select element.
  $delivery_handlers = gk_exports_delivery_handlers();
  $delivery_handler_options = array();

  if (!$delivery_handler_default) {
    $delivery_handler_options[] = '<None>';
  }

  foreach ($delivery_handlers as $machine_name => $info) {
    $delivery_handler_options[$machine_name] = $info['class']::getName();
  }

  $subform['ajax_wrapper']['handler'] = array(
    '#type' => 'select',
    '#options' => $delivery_handler_options,
    '#default_value' => $delivery_handler_default,
    '#ajax' => array(
      'callback' => 'gk_exports_delivery_handler_ajax_select_form_callback',
      'wrapper' => 'ajax-wrapper--delivery',
    ),
  );

  // Handler options.
  $subform['ajax_wrapper']['options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Options',
  );

  if (!$delivery_handler_default) {
    $subform['ajax_wrapper']['options']['#description'] = 'Select a delivery handler to see available options.';
  }
  else {
    // We have a delivery handler so ask it for an options form. First work out
    // if we're passing default values to the form builder, though.
    $default_options = $defaults['handler'] == $delivery_handler_default ? $defaults['options'] : array();

    if ($delivery_handler_form = $delivery_handlers[$delivery_handler_default]['class']::optionsForm($default_options)) {
      $subform['ajax_wrapper']['options'] += $delivery_handler_form;
    }
    // Display a suitable message if the options form is blank.
    else {
      $subform['ajax_wrapper']['options']['#description'] = t('No options are available for the %name delivery handler.', array(
        '%name' => $delivery_handlers[$delivery_handler_default]['class']::getName(),
      ));
    }
  }

  return $form;
}

/**
 * AJAX callback for the delivery handler select element.
 */
function gk_exports_delivery_handler_ajax_select_form_callback($form, $form_state) {
  $parents = $form_state['gk_exports']['delivery_handler_ajax_select']['parents'];
  return drupal_array_get_nested_value($form, array_merge($parents, array('ajax_wrapper')));
}

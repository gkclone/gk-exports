<?php

abstract class ExportBuildHandler {
  protected $options;

  public function __construct($options = array()) {
    $this->options = $options;
  }

  /**
   *
   */
  public static function optionsForm() {
    // Date filters should make sense to all build handlers.
    $form['date_from'] = array(
      '#type' => 'date_popup',
      '#date_format' => 'Y-m-d',
      '#title' => t('From'),
    );

    $form['date_to'] = array(
      '#type' => 'date_popup',
      '#date_format' => 'Y-m-d',
      '#title' => t('To'),
    );

    return $form;
  }

  /**
   *
   */
  abstract public function getData();

  /**
   *
   */
  public static function getName() {
    return static::$name;
  }
}

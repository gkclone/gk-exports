<?php

abstract class GKCSVExportBuildHandler extends CSVExportBuildHandler {
  public function __construct($options = array()) {
    parent::__construct($options);
  }

  /**
   * Implements CSVExportBuildHandler::csvHeaderInfo().
   */
  public static function csvHeaderInfo() {
    return array(
      'source' => array(
        'handler' => 'GKSourceExportFieldHandler',
      ),
      'event' => array(
        'real field' => array(
          'reward_type',
          'reward_id',
          'data',
        ),
        'handler' => 'GKEventExportFieldHandler',
      ),
    );
  }
}

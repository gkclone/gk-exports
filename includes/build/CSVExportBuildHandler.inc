<?php

abstract class CSVExportBuildHandler extends ExportBuildHandler {
  public function __construct($options = array()) {
    parent::__construct($options);
  }

  /**
   *
   */
  abstract public static function csvHeaderInfo();

  /**
   * Helper function used to build the header row for a CSV export.
   */
  public static function csvBuildHeaderRow($csv_header_info) {
    $header_row = array();

    foreach ($csv_header_info as $name => $info) {
      $header_row[$name] = isset($info['title']) ? $info['title'] : $name;
    }

    return $header_row;
  }

  /**
   * Helper function used to build each row for a CSV export.
   */
  public static function csvBuildRows($csv_header_info, array $data) {
    $rows = array();
    $header_row = self::csvBuildHeaderRow($csv_header_info);

    foreach ($data as $record) {
      $record = (array) $record;
      $row = array();

      foreach ($csv_header_info as $name => $info) {
        $real_field = isset($info['real field']) ? $info['real field'] : NULL;
        $field_handler_instance = new $info['handler']($real_field, $record);

        $row[] = $field_handler_instance->handle();
      }

      $rows[] = array_combine(array_keys($header_row), $row);
    }

    return array_merge(array($header_row), $rows);
  }
}

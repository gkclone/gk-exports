<?php

abstract class ExportFieldHandler {
  private $context;
  private $real_values;

  public function __construct($real_fields, array $context) {
    $this->context = $context;

    if (!is_array($real_fields)) {
      $real_fields = array($real_fields);
    }

    foreach ($real_fields as $real_field) {
      if (isset($context[$real_field])) {
        $this->real_values[$real_field] = $context[$real_field];
      }
    }
  }

  protected function getContext() {
    return $this->context;
  }

  protected function getRealValues() {
    return $this->real_values;
  }

  abstract public function handle();
}

class DefaultExportFieldHandler extends ExportFieldHandler {
  public function __construct($real_fields, array $context) {
    parent::__construct($real_fields, $context);
  }

  public function handle() {
    return implode('-', $this->getRealValues());
  }
}

class GenderExportFieldHandler extends DefaultExportFieldHandler {
  protected static $title_to_gender = array(
    'mr' => 'm',
    'mrs' => 'f',
    'ms' => 'f',
    'miss' => 'f',
  );

  public function __construct($real_fields, array $context) {
    parent::__construct($real_fields, $context);
  }

  public function handle() {
    $real_values = $this->getRealValues();

    if (isset(self::$title_to_gender[$real_values['title']])) {
      return self::$title_to_gender[$real_values['title']];
    }
  }
}

class GKSourceExportFieldHandler extends DefaultExportFieldHandler {
  public function handle() {
    return variable_get('gk_exports_source');
  }
}

class GKEventExportFieldHandler extends DefaultExportFieldHandler {
  public function __construct($real_fields, array $context) {
    parent::__construct($real_fields, $context);
  }

  public function handle() {
    $real_values = $this->getRealValues();

    // A value in the data array will take precedence over anything else.
    if (!empty($real_values['data'])) {
      $data = unserialize($real_values['data']);

      if (!empty($data['gk_exports']['event'])) {
        return $data['gk_exports']['event'];
      }
    }

    // Default event format: <reward-type>-<reward-id>
    if (!empty($real_values['reward_type']) && !empty($real_values['reward_id'])) {
      return $real_values['reward_type'] . '-' . $real_values['reward_id'];
    }
  }
}

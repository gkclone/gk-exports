<?php

class ScreenExportDeliveryHandler extends ExportDeliveryHandler {
  protected static $name = 'Screen';

  public function __construct($deliverables, $options = array()) {
    parent::__construct($deliverables, $options);
  }

  /**
   * Implements ExportDeliveryHandler::deliver().
   */
  public function deliver() {
    if (!module_exists('devel')) {
      drupal_set_message('Module \'devel\' must be enabled to use the screen delivery handler.');
      return FALSE;
    }

    dpm($this->deliverables);
    return TRUE;
  }
}

<?php

class CSVFTPExportDeliveryHandler extends ExportDeliveryHandler {
  protected static $name = 'CSV: FTP';

  public function __construct($deliverables, $options = array()) {
    parent::__construct($deliverables, $options);
  }

  /**
   * Overrides ExportDeliveryHandler::optionsForm().
   */
  public static function optionsForm($defaults = array()) {
    $form['host'] = array(
      '#type' => 'textfield',
      '#title' => 'Host',
      '#default_value' => !empty($defaults['host']) ? $defaults['host'] : '',
    );

    $form['user'] = array(
      '#type' => 'textfield',
      '#title' => 'User',
      '#default_value' => !empty($defaults['user']) ? $defaults['user'] : '',
    );

    $form['pass'] = array(
      '#type' => 'textfield',
      '#title' => 'Pass',
      '#default_value' => !empty($defaults['pass']) ? $defaults['pass'] : '',
    );

    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => 'Path',
      '#default_value' => !empty($defaults['path']) ? $defaults['path'] : '',
    );

    $form['filename'] = array(
      '#type' => 'textfield',
      '#title' => 'Filename',
      '#description' => t('Leave blank to use the default pattern: %pattern', array(
        '%pattern' => '<source>--<Y-m-d>--<H-i-s>.csv',
      )),
      '#default_value' => !empty($defaults['filename']) ? $defaults['filename'] : '',
    );

    return $form;
  }

  /**
   * Implements ExportDeliveryHandler::deliver().
   */
  public function deliver() {
    // Check we actually have something to deliver.
    if (empty($this->deliverables)) {
      // Return NULL rather FALSE since delivery hasn't actually failed.
      return NULL;
    }

    // Read the options into variables for easier referencing.
    foreach (array('host', 'user', 'pass', 'path', 'filename') as $option) {
      $$option = $this->options[$option];
    }

    // Default filename pattern.
    if (empty($filename)) {
      $filename = '[gk-exports:source]--[current-date:custom:Y-m-d]--[current-date:custom:H-i-s].csv';
    }

    $filename = token_replace($filename);

    // Check we can make a connection to the FTP server.
    if ((!$connection = @ftp_connect($host)) || !ftp_login($connection, $user, $pass)) {
      return FALSE;
    }

    ftp_pasv($connection, TRUE);

    // Write the CSV data to the temporary file system.
    $temp_name = drupal_tempnam('temporary://', 'gk-exports-');
    $success = FALSE;

    if ($handle = fopen($temp_name, 'w')) {
      foreach ($this->deliverables as $row) {
        fputcsv($handle, (array) $row);
        ob_flush(); flush();
      }

      fclose($handle);

      // Transfer the file to the FTP server.
      $destination = '/' . $path . '/' . $filename;
      ftp_put($connection, $destination, $temp_name, FTP_ASCII) && $success = TRUE;

      // Delete the temporary file.
      file_unmanaged_delete($temp_name);
    }

    ftp_close($connection);
    return $success;
  }
}

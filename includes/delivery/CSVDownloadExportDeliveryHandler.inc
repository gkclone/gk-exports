<?php

class CSVDownloadExportDeliveryHandler extends ExportDeliveryHandler {
  protected static $name = 'CSV: Download';

  public function __construct($deliverables, $options = array()) {
    parent::__construct($deliverables, $options);
  }

  /**
   * Overrides ExportDeliveryHandler::optionsForm().
   */
  public static function optionsForm($defaults = array()) {
    $form['filename'] = array(
      '#type' => 'textfield',
      '#title' => t('Filename'),
      '#description' => t('Leave blank to use the default pattern: %pattern', array(
        '%pattern' => '<source>--<Y-m-d>--<H-i-s>.csv',
      )),
      '#default_value' => !empty($defaults['filename']) ? $defaults['filename'] : '',
    );

    return $form;
  }

  /**
   * Implements ExportDeliveryHandler::deliver().
   */
  public function deliver() {
    if (!$handle = fopen('php://output', 'w')) {
      return;
    }

    // Default filename pattern.
    $filename = '[gk-exports:source]--[current-date:custom:Y-m-d]--[current-date:custom:H-i-s].csv';

    if (!empty($this->options['filename'])) {
      $filename = $this->options['filename'];
    }

    $filename = token_replace($filename);

    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Content-Type: text/csv; charset=utf-8; encoding=utf-8');
    header('Content-Disposition: attachment; filename="' . $filename . '"');

    foreach ($this->deliverables as $row) {
      fputcsv($handle, (array) $row);
      ob_flush(); flush();
    }

    fclose($handle);
    exit;
  }
}

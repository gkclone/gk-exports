<?php

abstract class ExportDeliveryHandler {
  protected $deliverables;
  protected $options;

  public function __construct($deliverables, $options = array()) {
    $this->deliverables = $deliverables;
    $this->options = $options;
  }

  /**
   *
   */
  public static function optionsForm($defaults = array()) {
    // Blank by default.
  }

  /**
   *
   */
  abstract public function deliver();

  /**
   *
   */
  public static function getName() {
    return static::$name;
  }
}

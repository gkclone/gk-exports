<?php

class CSVSFTPExportDeliveryHandler extends CSVFTPExportDeliveryHandler {
  protected static $name = 'CSV: SFTP';

  public function __construct($deliverables, $options = array()) {
    parent::__construct($deliverables, $options);
  }

  /**
   * Overrides CSVFTPExportDeliveryHandler::deliver().
   */
  public function deliver() {
    // Check we actually have something to deliver.
    if (empty($this->deliverables)) {
      // Return NULL rather FALSE since delivery hasn't actually failed.
      return NULL;
    }

    // Read the options into variables for easier referencing.
    foreach (array('host', 'user', 'pass', 'path', 'filename') as $option) {
      $$option = $this->options[$option];
    }

    // Default filename pattern.
    if (empty($filename)) {
      $filename = '[gk-exports:source]--[current-date:custom:Y-m-d]--[current-date:custom:H-i-s].csv';
    }

    $filename = token_replace($filename);

    // Check we can make a connection to the SFTP server.
    if ((!$connection = @ssh2_connect($host)) || !ssh2_auth_password($connection, $user, $pass)) {
      return FALSE;
    }

    // Check we can open the SFTP subsystem.
    if (!$sftp = ssh2_sftp($connection)) {
      return FALSE;
    }

    // Open a handle to a file on the SFTP server and write the CSV data.
    $destination = '/' . $path . '/' . $filename;
    $success = FALSE;

    if ($handle = fopen('ssh2.sftp://' . $sftp . $destination, 'w')) {
      foreach ($this->deliverables as $row) {
        fputcsv($handle, (array) $row);
        ob_flush(); flush();
      }

      fclose($handle);
      $success = TRUE;
    }

    return $success;
  }
}

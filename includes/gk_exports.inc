<?php

/**
 * Get a list of export build handlers.
 */
function gk_exports_build_handlers() {
  if ($cache = cache_get('gk_exports_build_handlers')) {
    return $cache->data;
  }

  $handlers = module_invoke_all('gk_exports_build_handlers');
  drupal_alter('gk_exports_build_handlers', $handlers);
  cache_set('gk_exports_build_handlers', $handlers);

  return $handlers;
}

/**
 * Get a list of export delivery handlers.
 */
function gk_exports_delivery_handlers() {
  if ($cache = cache_get('gk_exports_delivery_handlers')) {
    return $cache->data;
  }

  $handlers = gk_exports_default_delivery_handlers();
  $handlers += module_invoke_all('gk_exports_delivery_handlers');

  drupal_alter('gk_exports_delivery_handlers', $handlers);
  cache_set('gk_exports_delivery_handlers', $handlers);

  return $handlers;
}

/**
 * Helper function for retrieving a build handler array.
 */
function gk_exports_get_build_handler($machine_name) {
  $handlers = gk_exports_build_handlers();

  if (isset($handlers[$machine_name])) {
    return $handlers[$machine_name];
  }
}

/**
 * Helper function for retrieving a delivery handler array.
 */
function gk_exports_get_delivery_handler($machine_name) {
  $handlers = gk_exports_delivery_handlers();

  if (isset($handlers[$machine_name])) {
    return $handlers[$machine_name];
  }
}

/**
 * Define a few default delivery handlers.
 */
function gk_exports_default_delivery_handlers() {
  return array(
    'screen' => array(
      'class' => 'ScreenExportDeliveryHandler',
    ),
    'csv_download' => array(
      'class' => 'CSVDownloadExportDeliveryHandler',
    ),
    'csv_ftp' => array(
      'class' => 'CSVFTPExportDeliveryHandler',
    ),
    'csv_sftp' => array(
      'class' => 'CSVSFTPExportDeliveryHandler',
    ),
  );
}

/**
 * Run an export.
 *
 * @param $build Array
 *   An associative array containing:
 *     handler: the machine name of a build handler
 *     options: an associative array of options (optional)
 *
 * @param $delivery Array
 *   An associative array containing:
 *     handler: the machine name of a delivery handler
 *     options: an associative array of options (optional)
 */
function gk_exports_export($build, $delivery) {
  if (!$build_handler = gk_exports_get_build_handler($build['handler'])) {
    drupal_set_message(t('Build handler %handler could not be found.', array(
      '%handler' => $build['handler'],
    )), 'warning');

    return;
  }

  if (!$delivery_handler = gk_exports_get_delivery_handler($delivery['handler'])) {
    drupal_set_message(t('Delivery handler %handler could not be found.', array(
      '%handler' => $delivery['handler'],
    )), 'warning');

    return;
  }

  // Could take a while.
  set_time_limit(0);

  // Create a new log record.
  $lid = gk_exports_log(NULL, array(
    'build' => $build + array(
      'start' => time(),
    ),
    'delivery' => $delivery,
  ));

  // Build.
  $options = isset($build['options']) ? $build['options'] : array();
  $build_handler_instance = new $build_handler['class']($options);

  $data = $build_handler_instance->getData();

  // Update the log record.
  $build_stop_delivery_start = time();

  $lid && gk_exports_log($lid, array(
    'build' => array(
      'stop' => $build_stop_delivery_start,
    ),
    'delivery' => array(
      'start' => $build_stop_delivery_start,
    ),
  ));

  // Delivery.
  $options = isset($delivery['options']) ? $delivery['options'] : array();
  $delivery_handler_instance = new $delivery_handler['class']($data, $options);

  if ($delivery_handler_instance->deliver()) {
    drupal_set_message('The export ran successfully.');
  }
  else {
    drupal_set_message('The export failed during delivery.', 'error');
  }

  // Update the log record.
  $lid && gk_exports_log($lid, array(
    'delivery' => array(
      'stop' => time(),
    ),
  ));
}

/**
 * Helper function for writing to the gk_exports_log database table.
 */
function gk_exports_log($lid, $info) {
  $record = $primary_keys = array();

  if (isset($lid)) {
    $record['lid'] = $lid;
    $primary_keys = array('lid');
  }

  foreach (array('build', 'delivery') as $type) {
    foreach (array('handler', 'options', 'start', 'stop') as $field) {
      if (isset($info[$type][$field])) {
        $record[$type . '_' . $field] = $info[$type][$field];
      }
    }
  }

  if (drupal_write_record('gk_exports_log', $record, $primary_keys) !== FALSE) {
    return $record['lid'];
  }
}

/**
 * Title callback for build/delivery handler pages.
 */
function gk_exports_handler_page_title_callback($type, $machine_name) {
  $function = 'gk_exports_get_' . $type . '_handler';

  if ($handler = $function($machine_name)) {
    return $handler['class']::getName();
  }

  // If we get here then just return the machine name of the handler.
  return $machine_name;
}
